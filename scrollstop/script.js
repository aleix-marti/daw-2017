





$(document).ready(function(){

    var current = 0;

    var top1 = $('#section1').offset().top;
    var top2 = $('#section2').offset().top;
    var top3 = $('#section3').offset().top;
    var top4 = $('#section4').offset().top;

    var lastScrollTop = 0;
    // element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.
    window.addEventListener("scroll", function() {
        // or window.addEventListener("scroll"....
        var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
        if (st > lastScrollTop) {
          // downscroll code
          if(st >= top1 && current<1) {
              $("#section1").scrollTop(0);
              refresh();
          }
          if(st >= top2 && current<2) {
              $("#section2").scrollTop(0);
              refresh();
          }
          if(st >= top3 && current<3) {

            $("#section3").scrollTop(0);
              refresh();
          }
          if(st >= top4 && current<4) {
              $("#section4").scrollTop(0);
              refresh();
          }
        } else {
          // upscroll code
        }
        lastScrollTop = st;
        // console.log("scroll:", lastScrollTop);
      }, false);


      function refresh() {
          current++;
          $("body").css({ overflow: "hidden", height: "100%" });
          setTimeout(function() {
            $("body").css({ overflow: "auto", height: "auto" });
            console.log("reset body");
          }, 3000);
      }
      
});
