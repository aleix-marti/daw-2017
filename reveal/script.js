
// Changing the defaults
window.sr = ScrollReveal({ reset: true });

// Customizing a reveal set
sr.reveal(".foo", {
  duration: 2000,
  origin: "bottom"
});
sr.reveal(".bar", {
  duration: 500,
  origin: "right"
});